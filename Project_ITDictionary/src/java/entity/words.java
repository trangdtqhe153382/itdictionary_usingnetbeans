/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author dell
 */
public class words {
    int id;
    String keyword;
    String defination;
    Boolean isPublic;
    Boolean isActive;
    String created_by;

    public words() {
    }

    public words(int id, String keyword, String defination, Boolean isPublic, Boolean isActive, String created_by) {
        this.id = id;
        this.keyword = keyword;
        this.defination = defination;
        this.isPublic = isPublic;
        this.isActive = isActive;
        this.created_by = created_by;
    }

    public int getId() {
        return id;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getDefination() {
        return defination;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void setDefination(String defination) {
        this.defination = defination;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @Override
    public String toString() {
        return "words{" + "id=" + id + ", keyword=" + keyword + ", defination=" + defination + ", isPublic=" + isPublic + ", isActive=" + isActive + ", created_by=" + created_by + '}';
    }
    
}
