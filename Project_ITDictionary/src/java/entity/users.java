/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Date;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class users {
    String username;
    String email;
    String phone;
    String fullname;
    String password;
    Boolean active;
    int role_id;

    public users() {
    }

    public users(String username, String email, String phone, String fullname, String password, Boolean active, int role_id) {
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.fullname = fullname;
        this.password = password;
        this.active = active;
        this.role_id = role_id;
    }
    private static final Logger LOG = Logger.getLogger(users.class.getName());

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getFullname() {
        return fullname;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getActive() {
        return active;
    }

    public int getRole_id() {
        return role_id;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    @Override
    public String toString() {
        return "users{" + "username=" + username + ", email=" + email + ", phone=" + phone + ", fullname=" + fullname + ", password=" + password + ", active=" + active + ", role_id=" + role_id + '}';
    }

    
}
