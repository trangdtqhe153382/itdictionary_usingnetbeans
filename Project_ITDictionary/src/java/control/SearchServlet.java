/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package control;

import dao.DAO;
import entity.users;
import entity.words;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dell
 */
@WebServlet(name = "SearchServlet", urlPatterns = {"/search"})
public class SearchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String txtSearch = request.getParameter("txt");

        DAO dao = new DAO();
        words wordByName = dao.getWordByName(txtSearch);
        List<words> list = dao.searchByName(txtSearch);
        if (wordByName != null) {
            String username = wordByName.getCreated_by();
            users u = dao.checkUserExist(username);
            request.setAttribute("wordByName", wordByName);
            if (u.getRole_id() != 1) {
                request.setAttribute("by_user", "Contributed by user!");
            }

            if (list.isEmpty()) {
                request.setAttribute("errormessagesimilar", "No data found");
            } else {
                request.setAttribute("listWordRelate", list);
            }
        } else if (wordByName == null) {
            request.setAttribute("errorword", txtSearch + ":");
            request.setAttribute("errormessage", "No data found");
         
        }

//        if (wordByName == null) {
//            request.setAttribute("errorword", txtSearch + ":");
//            request.setAttribute("errormessage", "No data found");
//        } else {
//            request.setAttribute("wordByName", wordByName);
//            if (u.getRole_id() != 1) {
//                request.setAttribute("by_user", "Contributed by user!");
//            }
//        }

        if (list.isEmpty()) {
            request.setAttribute("errormessagesimilar", "No data found");
        } else {
            request.setAttribute("listWordRelate", list);
        }

        request.setAttribute("txtS", txtSearch);
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
