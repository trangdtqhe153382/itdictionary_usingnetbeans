/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import entity.users;
import entity.words;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dell
 */
public class DAO {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

//    public List<Words> getAllWord() {
//        List<Words> list = new ArrayList<>();
//        String query = "select * from words";
//        try {
//            conn = new DBContext().getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(new Words(rs.getInt(1),
//                        rs.getString(2),
//                        rs.getString(3),
//                        rs.getString(4),
//                        rs.getBoolean(5),
//                        rs.getInt(6),
//                        rs.getBoolean(7),
//                        rs.getString(8),
//                        rs.getDate(9),
//                        rs.getString(10),
//                        rs.getDate(11),
//                        rs.getString(12)));
//            }
//        } catch (Exception e) {
//        }
//        return list;
//    }

//    public List<Categories> getAllCategory() {
//        List<Categories> list = new ArrayList<>();
//        String query = "select * from categories";
//        try {
//            conn = new DBContext().getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(new Categories(rs.getInt(1),
//                        rs.getString(2),
//                        rs.getDate(3),
//                        rs.getString(4),
//                        rs.getDate(5),
//                        rs.getString(6)));
//            }
//        } catch (Exception e) {
//        }
//        return list;
//    }

//    public Words getLast() {
//        String query = "select top 1 * from words\n"
//                + "order by id desc";
//        try {
//            conn = new DBContext().getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            rs = ps.executeQuery();
//            while(rs.next()){
//                return new Words(rs.getInt(1),
//                        rs.getString(2),
//                        rs.getString(3),
//                        rs.getString(4),
//                        rs.getBoolean(5),
//                        rs.getInt(6),
//                        rs.getBoolean(7),
//                        rs.getString(8),
//                        rs.getDate(9),
//                        rs.getString(10),
//                        rs.getDate(11),
//                        rs.getString(12));
//            }
//        } catch (Exception e) {
//        }
//        return null;
//    }

//    public List<Words> getPopularWord() {
//        List<Words> list = new ArrayList<>();
//        String query = "select top 20 * from words\n"
//                + "order by id desc";
//        try {
//            conn = new DBContext().getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(new Words(rs.getInt(1),
//                        rs.getString(2),
//                        rs.getString(3),
//                        rs.getString(4),
//                        rs.getBoolean(5),
//                        rs.getInt(6),
//                        rs.getBoolean(7),
//                        rs.getString(8),
//                        rs.getDate(9),
//                        rs.getString(10),
//                        rs.getDate(11),
//                        rs.getString(12)));
//            }
//        } catch (Exception e) {
//        }
//        return list;
//    }
    
//    public List<Words> getWordByCID(String cid) {
//        List<Words> list = new ArrayList<>();
//        String query = "SELECT t1.keyword\n" +
//                        "FROM words As t1, words_map_categories AS t2\n" +
//                        "WHERE t1.id=t2.word_id AND t2.category_id=?";
//        try {
//            conn = new DBContext().getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            ps.setString(1, cid);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(new Words(rs.getInt(1),
//                        rs.getString(2),
//                        rs.getString(3),
//                        rs.getString(4),
//                        rs.getBoolean(5),
//                        rs.getInt(6),
//                        rs.getBoolean(7),
//                        rs.getString(8),
//                        rs.getDate(9),
//                        rs.getString(10),
//                        rs.getDate(11),
//                        rs.getString(12)));
//            }
//        } catch (Exception e) {
//        }
//        return list;
//    }

    public List<words> searchByName(String txtSearch) {
        List<words> list = new ArrayList<>();
        String query = "select * from words\n"
                + "where [defination] like ? and publish = 'true' and active = 'true';";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1,"%"+ txtSearch+"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new words(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getBoolean(4),
                        rs.getBoolean(5),
                        rs.getString(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public words getWordByName(String name) {
        String query = "select * from words\n" +
                        "where keyword = ? and publish = 'true' and active = 'true';";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new words(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getBoolean(4),
                        rs.getBoolean(5),
                        rs.getString(6));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public words getWordByID(String id) {
        String query = "select * from words\n"
                + "where id = ? and publish = 'true' and active = 'true';";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new words(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getBoolean(4),
                        rs.getBoolean(5),
                        rs.getString(6));
            }
        } catch (Exception e) {
        }
        return null;
    }
//    
//    public int getNumberSearch(String name) {
//        String query = "select words.times from words\n"
//                + "where keyword = ?";
//        try {
//            conn = new DBContext().getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            ps.setString(1, name);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                return rs.getInt(1);
//            }
//        } catch (Exception e) {
//        }
//        return 0;
//    }
//    
//    public void addNumberSearch(String txtSearch, int numberPreSearch) {
//        String query = "UPDATE words \n" +
//                        "SET times = ? \n"  +
//                        "WHERE keyword = ?";
//        try {
//            conn = new DBContext().getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            ps.setInt(1, numberPreSearch + 1);
//            ps.setString(2, txtSearch);
//            ps.executeUpdate();
//        } catch (Exception e) {
//        }
//    }
//    
    public users checkUserExist(String username) {
        String query = "select * from users\n"
                + "where [username] = ?;";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new users(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getInt(7));
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public users login(String username, String password) {
        
        String query = "select * from users\n" +
                        "where [username] = ?\n" +
                        "and [password] = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();
            while(rs.next()) {
                return new users(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getInt(7));
            }
        } catch (Exception e) {
        }
        return null;
    }
    public void singup(String username, String email, String phone, String fullname, String password) {
        String query = "insert into users\n"
                + "values(?,?,?,?,?,?,?)";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, email);
            ps.setString(3, phone);
            ps.setString(4, fullname);
            ps.setString(5, password);
            ps.setBoolean(6, true);
            ps.setInt(7, 2);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<words> getWordByUser(String username) {
        List<words> list = new ArrayList<>();
        String query = "SELECT *\n" +
                        "FROM words\n" +
                        "WHERE words.created_by = ?;";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new words(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getBoolean(4),
                        rs.getBoolean(5),
                        rs.getString(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public void insertWord(String keyword, String defination, boolean isPublic, boolean isActive, String created_by) {
        String query = "INSERT INTO words (keyword, defination, publish, active, created_by)\n" +
                        "VALUES (?, ?, ?, ?, ?)";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, keyword);
            ps.setString(2, defination);
            ps.setBoolean(3, isPublic);
            ps.setBoolean(4, isActive);
            ps.setString(5, created_by);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public words getWordById(String id) {
        String query = "select * from words\n"
                + "where id = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new words(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getBoolean(4),
                        rs.getBoolean(5),
                        rs.getString(6));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void updateWord(String keyword, String defination, boolean checkPublic, boolean checkActive, String username, String id) {
        String query = "update words\n" +
                        "set [keyword] = ?,\n" +
                        "[defination] = ?,\n" +
                        "[publish] = ?,\n" +
                        "[active] = ?,\n" +
                        "[created_by] = ?\n" +
                        "where id = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, keyword);
            ps.setString(2, defination);
            ps.setBoolean(3, checkPublic);
            ps.setBoolean(4, checkActive);
            ps.setString(5, username);
            ps.setString(6, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void deleteWord(String id) {
        String query = "delete words\n" +
                        "where id = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateUser(String username, String email, String phone, String fullname, String password) {
        String query = "update users\n" +
                        "set [email] = ?,\n" +
                        "[phone] = ?,\n" +
                        "[fullname] = ?,\n" +
                        "[password] = ?\n" +
                        "where username = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, phone);
            ps.setString(3, fullname);
            ps.setString(4, password);
            ps.setString(5, username);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    

    
    
}
