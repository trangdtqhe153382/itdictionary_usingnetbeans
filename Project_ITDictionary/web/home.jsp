<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : Home
    Created on : Feb 7, 2023, 9:38:54 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Technology Dictionary</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>

            <div class="container">
                <div class="latest-product__text py-5">
                    <h4 class="text-center">Technology Dictionary</h4>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="hero__search">
                                <div class="hero__search__form">
                                    <form action="search">
                                        <input value="${txtS}" name="txt" type="text" placeholder="Enter a word">
                                    <button type="submit" class="site-btn">SEARCH</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <h3 class="font-weight-bold mb-1">${wordByName.keyword} </h3>
                <p class="text-red text-danger">${by_user}</p>
                <p>${wordByName.defination}</p>
                <h3 class="font-weight-bold mb-1">${errorword}</h3>
                <p class="text-red">${errormessage}</p>

                <h6 class="font-weight-bold mt-5">Similar words: </h6>
                <p class="text-red">${errormessagesimilar}</p>
                
                <div class="row">
                    <c:forEach var="o" items="${listWordRelate}">
                        <div class="col-md-3 mt-3">
                            <a href="search?txt=${o.keyword}" class="text-black-50 col-md-3 border-bottom bg-bluelight">${o.keyword}</a>
                        </div>
                    </c:forEach>
                </div>
                    
                <div class="container py-5">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="font-weight-bold">Technology Dictionary</ul>
                            <li>Free online dictionary</li>
                            <li>Provide the latest dictionary of information technology</li>
                            <li>With a massive word store and a smart word suggestion system, Technology Dictionary helps you look up quickly.</li>
                        </div>

                    </div>
                </div>
            </div>


        <jsp:include page="footer.jsp"></jsp:include>

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
    </body>

</html>
