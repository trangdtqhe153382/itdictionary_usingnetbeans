<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : Header
    Created on : Feb 9, 2023, 7:53:40 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Header Section Begin -->
<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <ul>
                            <li><i class="fa fa-envelope"></i>Technology Dictionary</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="header__top__right">
                        <c:if test="${sessionScope.user != null}">
                            <div class="header__top__right__auth mr-5">
                                <span class="mr-5 font-weight-bold"><a href="loadaccount?username=${sessionScope.user.username}">Hello ${sessionScope.user.username}!</a></span>
                                
                            </div>
                        </c:if>
                        
                    </div>
                </div>
                <div class="col-lg-2 col-md-2">
                    <div class="header__top__right">
                        <c:if test="${sessionScope.user != null}">
                            <div class="header__top__right__auth mr-5">
                                <a href="logout"><i class="fa fa-user"></i> Logout</a>
                            </div>
                        </c:if>
                        <c:if test="${sessionScope.user == null}">
                            <div class="header__top__right__auth">
                                <a href="login.jsp"><i class="fa fa-user"></i> Login</a>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="header__logo">
                    <a href="/home"><img src="img/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="header__menu">
                    <ul>
                        <li class="active"><a href="./home">Home</a></li>
                        <c:if test="${sessionScope.user != null}">
                            <li><a href="managedictionary">Manage Dictionary</a></li>
                        </c:if>
                        <li><a href="./contact.jsp">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- Header Section End -->
