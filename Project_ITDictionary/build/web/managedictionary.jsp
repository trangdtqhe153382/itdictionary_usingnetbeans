

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
    <body>
        <jsp:include page="header.jsp"></jsp:include>

            <div class="container">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Manage <b>Dictionary</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <a data-target="#addDictionaryModal"  class="btn btn-success float-right " data-toggle="modal"><i class="material-icons">&#xE147;</i> <span class="text-white">Add New Dictionary</span></a>
<!--                                <a data-target="#deleteAllDictionaryModal" class="btn btn-danger float-right" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span class="text-white">Delete</span></a>	-->
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
<!--                                <th>
                                    <span class="custom-checkbox">
                                        <input type="checkbox" id="selectAll">
                                        <label for="selectAll"></label>
                                    </span>
                                </th>-->
                                <th>ID</th>
                                <th>Key word</th>
                                <th>Defination</th>
                                <th>Public</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${listP}" var="o">
                            <tr>
<!--                                <td>
                                    <span class="custom-checkbox">
                                        <input type="checkbox" id="checkbox1" name="options[]" value="1">
                                        <label for="checkbox1"></label>
                                    </span>
                                </td>-->
                                <td>${o.id}</td>
                                <td>${o.keyword}</td>
                                <td>${o.defination}</td>
                                <td>${o.isPublic}</td>
                                <td>${o.isActive}</td>
                                <td>
                                    <a href="deletedictionary?id=${o.id}" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span class="text-white">Delete</span></a>
                                    <a href="loaddictionary?id=${o.id}"  class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span class="text-white">Update</span></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>
            <a href="home"><button type="button" class="btn btn-primary">Back to home</button>

        </div>
        <!--        </form>-->
        <!-- Edit Modal HTML -->
        <div id="addDictionaryModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="adddictionary" method="post">
                        <div class="modal-header">						
                            <h4 class="modal-title">Add Dictionary</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					
                            <div class="form-group">
                                <label>Keyword</label>
                                <input name="keyword" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Defination</label>
                                <textarea name="defination" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="public">Public</label>
                                <input checked type="checkbox" name="public" value="ON" /><br>
                            </div>
                            <div class="form-group">
                                <label for="active">Active</label>
                                <input checked type="checkbox" name="active" value="ON" /><br>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" value="Add">
                        </div>
                    </form>
                </div>
            </div>
        </div>

<!--        <div id="deleteAllEmployeeModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="deletealldictionary" method="post">
                        <div class="modal-header">						
                            <h4 class="modal-title">Delete All Dictionary</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					
                            <div class="form-group">
                                <p>Are you sure you want to clear your list of dictionaries?</p>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" value="Delete">
                        </div>
                    </form>
                </div>
            </div>
        </div>-->

        <script src="js/manager.js" type="text/javascript"></script>

    </body>
</html>