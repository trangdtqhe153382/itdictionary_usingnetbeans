<%-- 
    Document   : register
    Created on : Feb 7, 2023, 10:16:38 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
        <div class="login-form">
            <form action="sigup" method="post">
                <h2 class="text-center">Register</h2> 
                
                <p class="text-danger">${mess}</p>
                
                <div class="form-group">
                    Username:
                    <input name="username" type="text" class="form-control" placeholder="Username *" required="required">
                </div>
                <div class="form-group">
                    Fullname: 
                    <input name="fullname" type="text" class="form-control" placeholder="Fullname *" required="required">
                </div>
                <div class="form-group">
                    Email:
                    <input name="email" type="text" class="form-control" placeholder="Email *" required="required">
                </div>
                <div class="form-group">
                    Phone:
                    <input name="phone" type="text" class="form-control" placeholder="Phone *" required="required">
                </div>
                
                <div class="form-group">
                    Password:
                    <input name="pass" type="password" class="form-control" placeholder="Password *" required="required">
                </div>
                <div class="form-group">
                    Re-Password:
                    <input name="repass" type="password" class="form-control" placeholder="Re-Password *" required="required">
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Sig up</button>
                </div>
            </form>
            <p class="text-center"><a href="home">Back home</a></p>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>
    </body>
    </body>
</html>
