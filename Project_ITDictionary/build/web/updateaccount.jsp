

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
            <div class="container">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Update <b>Profile</b></h2>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-dialog ">
                    <div class="modal-content p-5">
                        <form action="updateaccount" method="post">
                            <h2 class="text-center">Update Profile</h2> 

                            <p class="text-danger">${mess}</p>

                        <div class="form-group">
                            Username: <input value="${acc.username}" name="username" type="text" class="form-control" placeholder="Username *" required="required" readonly>
                        </div>
                        <div class="form-group">
                            Fullname: <input value="${acc.fullname}" name="fullname" type="text" class="form-control" placeholder="Fullname *" required="required">
                        </div>
                        <div class="form-group">
                            Email: <input value="${acc.email}" name="email" type="text" class="form-control" placeholder="Email *" required="required">
                        </div>
                        <div class="form-group">
                            Phone: <input value="${acc.phone}" name="phone" type="text" class="form-control" placeholder="Phone *" required="required">
                        </div>
                        <div class="form-group">
                            Password: <input value="${acc.password}" name="pass" type="password" class="form-control" placeholder="Password *" required="required">
                        </div>
                        <div class="form-group">
                            Re-Password: <input value="${acc.password}" name="repass" type="password" class="form-control" placeholder="Re-Password *" required="required">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <p class="text-center"><a href="home">Back home</a></p>



        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>
