use project_PRJ301;

CREATE TABLE roles (
	id int IDENTITY(1,1) PRIMARY KEY,
	name nvarchar(50) NOT NULL
);

CREATE TABLE users (
	username varchar(50) NOT NULL PRIMARY KEY,
	email varchar(50) NOT NULL,
	phone varchar(13) NOT NULL,
	fullname nvarchar(255) NOT NULL,
	[password] varchar(50) NOT NULL,
	active bit NOT NULL,
	role_id int FOREIGN KEY REFERENCES roles(id)
);

CREATE TABLE words (
	id int IDENTITY(1,1) PRIMARY KEY,
	keyword nvarchar(100) NOT NULL,
	defination nvarchar(255) NOT NULL,
	publish bit NOT NULL,
	active bit NOT NULL,
	created_by varchar(50) FOREIGN KEY REFERENCES users(username)
);

INSERT INTO roles(name)
VALUES ('admin'), ('user');

INSERT INTO users(username, email, phone, fullname, [password], active, role_id)
VALUES ('admin', 'admin@gmail.com', '0123456789', 'Admin Nguyen', '123', 1, 1),
('user01', 'user01@gmail.com', '0123456789', 'User01 Nguyen', '123', 1, 2),
('user02', 'user02@gmail.com', '0123456789', 'User02 Nguyen', '123', 2, 2),
('user03', 'user03@gmail.com', '0123456789', 'User03 Nguyen', '123', 1, 2),
('user04', 'user04@gmail.com', '0123456789', 'User04 Nguyen', '123', 1, 2),
('user05', 'user05@gmail.com', '0123456789', 'User05 Nguyen', '123', 2, 2);

INSERT INTO words(keyword, defination, publish, active, created_by)
VALUES ('word01', 'word01 defination', 1, 1, 'admin'),
('word01', 'word01 defination', 1, 1, 'admin'),
('word02', 'word02 defination', 1, 0, 'admin'),
('word03', 'word03 defination', 0, 1, 'admin'),
('word04', 'word04 defination', 1, 1, 'user01'),
('word05', 'word05 defination', 1, 1, 'user05');